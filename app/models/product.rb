class Product < ApplicationRecord
  validate :cost_multiple_of_five
  belongs_to :seller, class_name: 'User', foreign_key: 'seller_id'

  def cost_multiple_of_five
    if cost % 5 != 0
      errors.add(:discount, 'Must be multiple of 5')
    end
  end
end
