class User < ApplicationRecord
  has_secure_password

  has_many :products, class_name: 'Product', foreign_key: 'seller_id'
end
