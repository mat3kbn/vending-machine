module VendingMachineService
  extend Dry::Monads[:result]

  AVAILABLE_COINS = [5, 10, 20, 50, 100]

  class << self
    def deposit(user:, amount:)
      return Failure(:buyer_role_missing) if user.buyer_role == false
      return Failure(:invalid_amount_value) if AVAILABLE_COINS.include?(amount) == false

      user.deposit += amount

      if user.save
        Success()
      else
        Failure(user.error_messages)
      end
    end

    def buy(user:, amount:, product_id:)
      return Failure(:buyer_role_missing) if user.buyer_role == false

      product = Product.find_by(id: product_id)
      return Failure(:product_not_found) if product.nil?
      return Failure(:insufficient_product_quantity) if amount > product.amount_available

      total_spent = amount * product.cost
      return Failure(:insufficient_deposit) if total_spent > user.deposit

      change = user.deposit - total_spent

      ActiveRecord::Base.transaction do
        user.deposit = 0
        product.amount_available -= amount

        user.save!
        product.save!
      end

      result = {
        total_spent: total_spent,
        change: value_in_coins(change),
        product: product.attributes.except('created_at', 'updated_at'),
      }
      Success(result)
    end

    def reset(user:)
      user.deposit = 0

      if user.save
        Success
      else
        Failure(user.error_messages)
      end
    end

    private

    def value_in_coins(value)
      result = []

      remaining_value = value
      AVAILABLE_COINS.sort { |a, b| b <=> a }.each do |coin_value|
        coin_amount = remaining_value / coin_value

        if coin_amount > 0
          remaining_value = remaining_value - (coin_amount * coin_value)
          result += Array.new(coin_amount, coin_value)
        end
      end

      result
    end
  end
end
