class VendingMachineController < ApplicationController
  before_action :authenticate!

  def deposit
    result = VendingMachineService.deposit(
      user: @current_user,
      amount: params[:amount],
    )

    if result.success?
      render json: {}, status: 200
    else
      render json: { error_message: result.failure }, status: 422
    end
  end

  def buy
    result = VendingMachineService.buy(
      user: @current_user,
      amount: params[:amount],
      product_id: params[:product_id],
    )

    if result.success?
      render json: result.value!, status: 200
    else
      render json: { error_message: result.failure }, status: 422
    end
  end

  def reset
    result = VendingMachineService.reset(
      user: @current_user,
    )

    if result.success?
      render json: {}, status: 200
    else
      render json: { error_message: result.failure }, status: 422
    end
  end
end
