class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods

  def authenticate!
    authenticate_or_request_with_http_basic do |username, password|
      user = User.find_by(name: username)
      if user && user.authenticate(password)
        @current_user = user
        true
      else
        false
      end
    end
  end
end
