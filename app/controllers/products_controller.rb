class ProductsController < ApplicationController
  before_action :authenticate!

  def index
    render json: Product.all
  end

  def show
    render json: Product.find(params[:id])
  end

  def create
    render json: Product.create(product_params)
  end

  def destroy
    product = Product.find(params[:id])
    authorize!(product)


    render json: product.destroy
  end

  def update
    product = Product.find(params[:id])
    authorize!(product)

    product.update(product_params)

    render json: product
  end

  private

  def product_params = params.require(:product).permit(:name, :cost, :amount_available, :seller_id)

  def authorize!(product)
    if product.seller != @current_user
      raise ActionController::RoutingError.new('Not Found')
    end
  end
end
