class UsersController < ApplicationController
  before_action :authenticate!
  skip_before_action :authenticate!, only: [:create]

  def index
    render json: User.all
  end

  def show
    render json: User.find(params[:id])
  end

  def create
    render json: User.create(user_params)
  end

  def destroy
    render json: User.destroy(params[:id])
  end

  def update
    user = User.find(params[:id])
    user.update(user_params)

    render json: user
  end

  private

  def user_params = params.require(:user).permit(:name, :password, :deposit, :buyer_role, :seller_role)
end
