class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :password
      t.integer :deposit
      t.boolean :buyer_role
      t.boolean :seller_role

      t.timestamps
    end
  end
end
