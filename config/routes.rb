Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  scope :api do
    resources :users
    resources :products

    post :deposit, to: 'vending_machine#deposit'
    post :reset, to: 'vending_machine#reset'
    post :buy, to: 'vending_machine#buy'
  end
end
