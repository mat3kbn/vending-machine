require "test_helper"

class ProductsControllerTest < ActionDispatch::IntegrationTest
  test '/products missing basic_auth headers' do
    get products_url
    assert_response :unauthorized
  end

  test '/products happy path' do
    headers = {
      Authorization: ActionController::HttpAuthentication::Basic.encode_credentials('buyer', 'secret')
    }

    get products_url, headers: headers
    assert_response :success
  end
end
