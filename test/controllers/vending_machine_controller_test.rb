require "test_helper"

class VendingMachineControllerTest < ActionDispatch::IntegrationTest
  def auth_header(user='buyer', pass='secret')
    credentials = ActionController::HttpAuthentication::Basic.encode_credentials(
      user,
      pass,
    )

    {
      Authorization: credentials
    }
  end

  def headers
    { ** auth_header }
  end

  class DepositEndpointTest < VendingMachineControllerTest
    test "happy path" do
      params = { amount: 5 }
      post deposit_url, params: params, headers: headers, as: :json

      assert_response :success
    end

    test 'non buyer role' do
      headers = { ** auth_header('seller', 'secret') }
      params = { amount: 5 }
      post deposit_url, params: params, headers: headers, as: :json

      assert_response :unprocessable_entity
      assert_equal({ 'error_message' => 'buyer_role_missing' }, response.parsed_body)
    end

    test 'invalid amount' do
      params = { amount: 4 }
      post deposit_url, params: params, headers: headers, as: :json

      assert_response :unprocessable_entity
      assert_equal({ 'error_message' => 'invalid_amount_value' }, response.parsed_body)
    end
  end

  class BuyEndpointTest < VendingMachineControllerTest
    test "happy path" do
      params = { amount: 1, product_id: products(:pepsi).id }
      post buy_url, params: params, headers: headers, as: :json

      assert_response :success

      expected_body = {
        "total_spent"=>5,
        "change"=>[50, 20, 20, 5],
        "product"=>{
          "amount_available"=>0,
          "id"=>394627963,
          "cost"=>5,
          "name"=>"Pepsi",
          "seller_id"=>1
        }
      }
      assert_equal(expected_body, response.parsed_body)
    end

    test 'non buyer role' do
      headers =  { ** auth_header('seller', 'secret') }
      params = { amount: 1, product_id: products(:pepsi).id }
      post buy_url, params: params, headers: headers, as: :json

      assert_response :unprocessable_entity
      assert_equal({ 'error_message' => 'buyer_role_missing' }, response.parsed_body)
    end

    test 'missing product' do
      params = { amount: 1, product_id: 12345 }
      post buy_url, params: params, headers: headers, as: :json

      assert_response :unprocessable_entity
      assert_equal({ 'error_message' => 'product_not_found' }, response.parsed_body)
    end

    test "insufficient product amount" do
      params = { amount: 2, product_id: products(:pepsi).id }
      post buy_url, params: params, headers: headers, as: :json

      assert_response :unprocessable_entity
      assert_equal({ 'error_message' => 'insufficient_product_quantity' }, response.parsed_body)
    end

    test "insufficient deposit" do
      headers =  { ** auth_header('buyer_wo_deposit', 'secret') }
      params = { amount: 1, product_id: products(:pepsi).id }
      post buy_url, params: params, headers: headers, as: :json

      assert_response :unprocessable_entity
      assert_equal({ 'error_message' => 'insufficient_deposit' }, response.parsed_body)
    end
  end
end
